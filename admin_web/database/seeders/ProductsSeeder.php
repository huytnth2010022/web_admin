<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('products')->truncate();
        DB::table('products')->insert([
            [
                'name' => 'LIBRE EAU DE PARFUM',
                'price' => '800000',
                'image' => 'https://luzo.vn/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2020/10/ban-chan-sat-m2c23-1.jpg.webp',

            ],
            [
                'name' => 'LIBRE EAU DE PARFUM',
                'price' => '800000',
                'image' => 'https://luzo.vn/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2020/10/ban-chan-sat-m2c23-1.jpg.webp',

            ],
            [
                'name' => 'LIBRE EAU DE PARFUM',
                'price' => '800000',
                'image' => 'https://luzo.vn/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2020/10/ban-chan-sat-m2c23-1.jpg.webp',

            ],
            [
                'name' => 'LIBRE EAU DE PARFUM',
                'price' => '800000',
                'image' => 'https://luzo.vn/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2020/10/ban-chan-sat-m2c23-1.jpg.webp',

            ],
            [
                'name' => 'LIBRE EAU DE PARFUM',
                'price' => '800000',
                'image' => 'https://luzo.vn/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2020/10/ban-chan-sat-m2c23-1.jpg.webp',

            ],



        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}
