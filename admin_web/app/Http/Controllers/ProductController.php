<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function getDetail(Request $request)
    {
        if ($request->input("search") != null) {
            $id = $request->input("search");
           $products =  DB::table('products')
               ->where('id', '=', $id)
               ->get();
          return view('products', ['products' => $products]);

        } else {
            $products = Product::all();
            return view('products', ['products' => $products]);

        }
    }
    public function add(){
        return view("add");
    }
    public function store(Request $request){
        $request->request->remove('_token');
        $name =$request->name;
        $price =$request->price;
        $image=$request->image;
        $product = new Product();
$product->name = $name;
$product->price = $price;
$product->image = $image;
        $product->timestamps=false;
        $product->save();
        return redirect('product');
    }

}
