<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Laravel</title>

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

<!-- Styles -->
<style>

    * {box-sizing: border-box;}

        body {
         margin: 0;
         font-family: Arial, Helvetica, sans-serif;
}

        .topnav {
         overflow: hidden;
         background-color: #e9e9e9;
         margin-bottom: 100px
}

        .topnav a {
         float: left;
         display: block;
         color: black;
         text-align: center;
         padding: 14px 16px;
         text-decoration: none;
         font-size: 17px;
}

        .topnav a:hover {
         background-color: #ddd;
         color: black;
}

        .topnav a.active {
         background-color: #2196F3;
         color: white;
}

        .topnav input[type=text] {
         float: right;
         padding: 6px;
         margin-top: 8px;
         margin-right: 16px;
         border: none;
         font-size: 17px;
}
        .topnav form button{
         float: right;
         padding: 6px;
         margin-top: 8px;
         margin-right: 16px;
         font-size: 17px;
}

        @media screen and (max-width: 600px) {
             .topnav a, .topnav input[type=text] {
             float: none;
             display: block;
             text-align: left;
             width: 100%;
             margin: 0;
             padding: 14px;
         }

             .topnav input[type=text] {
             border: 1px solid #ccc;
         }
}
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #04AA6D;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}


        body {
         font-family: 'Nunito', sans-serif;
}
        </style>

        </head>
        <body>

        <div class="topnav">
        <a  href="/product">Home</a>
        <a href="/add">Add Product</a>


        </div>
    <div class="container">
    <form action="/add" method="POST">
        @csrf
    <label for="fname">Product Name</label>
    <input type="text" id="fname" name="name" placeholder=" Product">

    <label for="lname">Price</label>
    <input type="text" id="lname" name="price" placeholder="Price">

    <label for="country">Image URL</label>
    <input type="text" id="lname" name="image" placeholder="img URL">


    

    <input type="submit" value="Submit">
    </form>
    </div>

        </body>
        </html>
