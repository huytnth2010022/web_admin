<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/product',[\App\Http\Controllers\ProductController::class, 'getDetail']);

Route::get('/add',[\App\Http\Controllers\ProductController::class, 'add']);
Route::post('/add',[\App\Http\Controllers\ProductController::class, "store"]);
